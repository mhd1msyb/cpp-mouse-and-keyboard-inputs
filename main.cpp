#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"

#include <iostream>

using namespace ci;
using namespace ci::app;
using namespace gl;

class Test: public App {
public:
  void mouseMove(MouseEvent mouse_event) override;
  void mouseDown(MouseEvent mouse_event) override;
  void mouseUp(MouseEvent mouse_event) override;
  void mouseWheel(MouseEvent mouse_event) override;
  void draw() override;

};

void app_settings(Test::Settings* settings){
}


void Test::mouseMove(MouseEvent mouse_event){
  std::cout << "x pos: " <<mouse_event.getX() <<"y pos: " <<mouse_event.getY() << '\n';
}

void Test::mouseWheel(MouseEvent mouse_event){
  std::cout << mouse_event.getWheelIncrement() << '\n';
}

void Test::mouseUp(MouseEvent mouse_event){
  std::cout << mouse_event.isLeft() <<" left release" << '\n';
  std::cout << mouse_event.isMiddle() <<" middle release"<< '\n';
  std::cout << mouse_event.isRight() << " right release"<<'\n';
}


void Test::mouseDown(MouseEvent mouse_event){
  std::cout << mouse_event.isLeft() <<" left pressed" << '\n';
  std::cout << mouse_event.isMiddle() <<" middle pressed" <<'\n';
  std::cout << mouse_event.isRight() << " right pressed"<<'\n';
}

void Test::draw(){
  clear(ci::Color::black(), true);
}

CINDER_APP(Test, RendererGl, app_settings)
